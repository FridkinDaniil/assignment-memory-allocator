#define _DEFAULT_SOURCE

#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>

#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"
#include "util.h"

struct block_header* get_block_header(void* mem){
    return (struct block_header*) (((uint8_t*) mem)-offsetof(struct block_header, contents));
} 

int main() {
    void* heap = heap_init(10000);
    if (heap != NULL) {
        debug_heap (stderr, heap);
    } else {
        err ("Ошибка при создании кучи\n");
    }
    struct block_header* first_block = (struct block_header*) heap;

    debug("Тест 1:\n");
    void* test1 = _malloc(2000);
    if (test1 == NULL) {
        err("Тест 1 не пройден\n");
    }
    if (first_block->capacity.bytes != 2000) {
        err("Тест 1 не пройден\n");
    }
    if (first_block->is_free) {
        err("Тест 1 не пройден\n");
    }
    debug_heap (stderr, heap);
    debug ("Тест 1 пройден\n");

    debug("Тест 2:\n");
    void* test2 = _malloc(2000);
    _free(test2);

    if (!get_block_header(test2)->is_free) {
        err("Тест 2 не пройден\n");
    }
    debug_heap (stderr, heap);
    debug ("Тест 2 пройден\n");

    debug("Тест 3:\n");
    void* test3_1 = _malloc(2000);
    void* test3_2 = _malloc(2000);
    _free(test3_1);
    _free(test3_2);

    if (!get_block_header(test3_1)->is_free) {
        err("Тест 3 не пройден\n");
    }
    if (!get_block_header(test3_2)->is_free) {
        err("Тест 3 не пройден\n");
    }
    debug_heap (stderr, heap);
    debug ("Тест 3 пройден\n");

    debug("Тест 4:\n");
    void* test4 = _malloc(10000);

    if (get_block_header(test4)->is_free) {
        err("Тест 4 не пройден\n");
    }
    if (get_block_header(test4)->capacity.bytes != 10000){
        err("Тест 4 не пройден\n");
    }
    debug_heap (stderr, heap);
    debug ("Тест 4 пройден\n");

    debug("Тест 5:\n");
    struct block_header* last = first_block;
    while (last->next != NULL) {
        last = last->next;
    }
    void* test5_mem_addr = last + size_from_capacity(last->capacity).bytes;
    void* test5_mem = mmap( (uint8_t*) (getpagesize() * ((size_t) test5_mem_addr / getpagesize() + (((size_t) test5_mem_addr % getpagesize()) > 0))),
        1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void* test5 = _malloc(10000);
    if (get_block_header(test5)->is_free){
        err("Тест 5 не пройден\n");
    }
    if (get_block_header(test5)->capacity.bytes != 10000){
        err("Тест 5 не пройден\n");
    }
    if (last == get_block_header(test5)) {
        err("Тест 5 не пройден\n");
    }
    if (test5 == test5_mem) {
        err("Тест 5 не пройден\n");
    }
    debug_heap (stderr, heap);
    debug ("Тест 5 пройден\n");

    debug("Все тесты пройдены\n");
}

